<?php

namespace Kanboard\Plugin\Boardnotes\Controller;

use Kanboard\Controller\BaseController;

class BoardnotesController extends BaseController
{

    public function BoardnotesShowProject()
    {
        $project = $this->getProject();
        $user = $this->getUser();

        $data = $this->boardnotesModel->modelBoardnotesShowProject($project['id'], $user['id']);
	$projectAccess = $this->boardnotesModel->modelBoardnotesGetProjectid($user['id']);
	$categories = $this->boardnotesModel->modelBoardnotesGetCategories($project['id']);

        return $this->response->html($this->helper->layout->project('Boardnotes:boardnotes/show', array('title' => t('Boardnotes'),
            'project' => $project,
            'data' => $data,
	    'categories' => $categories,
        )));
    }

    public function BoardnotesShowProjectRefresh()
    {
        $project = $this->getProject();
        $user = $this->getUser();

        $data = $this->boardnotesModel->modelBoardnotesShowProject($project['id'], $user['id']);
	$categories = $this->boardnotesModel->modelBoardnotesGetCategories($project['id']);

        return $this->response->html($this->helper->layout->app('Boardnotes:boardnotes/dataSingle', array('title' => t('Boardnotes'),
            'project' => $project,
            'data' => $data,
	    'categories' => $categories,
        )));
    }


    public function BoardnotesShowAll()
    {
        //$project = $this->getProject();
        $user = $this->getUser();

	$projectAccess = $this->boardnotesModel->modelBoardnotesGetProjectid($user['id']);
        $data = $this->boardnotesModel->modelBoardnotesShowAll($projectAccess, $user['id']);

        return $this->response->html($this->helper->layout->dashboard('Boardnotes:boardnotes/showAll', array('title' => t('Boardnotes'),
            'project' => 'Notes',
	    'projectAccess' => $projectAccess,
            //'project' => $project,
            'data' => $data,
	    'allProjects' => '1',
        )));
    }


    public function BoardnotesShowAllRefresh()
    {
        $project = $this->getProject();
        $user = $this->getUser();

	$projectAccess = $this->boardnotesModel->modelBoardnotesGetProjectid($user['id']);
        $data = $this->boardnotesModel->modelBoardnotesShowAll($projectAccess, $user['id']);

        return $this->response->html($this->helper->layout->app('Boardnotes:project_overview/data', array('title' => t('Boardnotes'),
            'project' => $project,
            'data' => $data,
        )));
    }



    public function BoardnotesDelete()
    {
        $project = $this->getProject();
        $user = $this->getUser();
        $note_id = $this->request->getStringParam('note_id');


        $validation = $this->boardnotesModel->modelBoardnotesDeleteNote($note_id, $user['id']);
    }


    public function BoardnotesDeleteAllDone()
    {
        $project = $this->getProject();
        $user = $this->getUser();
        $note_id = $this->request->getStringParam('project_id');


        $validation = $this->boardnotesModel->modelBoardnotesDeleteAllDone($project['id'], $user['id']);
    }


    public function BoardnotesUpdate()
    {
	$note_id = $this->request->getStringParam('note_id');
	$is_active = $this->request->getStringParam('is_active');
	$title = $this->request->getStringParam('title');
	$description = $this->request->getStringParam('description');
	$category = $this->request->getStringParam('category');
        $project = $this->getProject();
        $user = $this->getUser();

        $validation = $this->boardnotesModel->modelBoardnotesUpdateNote($user['id'], $note_id, $is_active, $title, $description, $category); 
    }


    public function BoardnotesAdd($is_active, $title, $description, $category)
    {
	$project = $this->getProject();
        $user = $this->getUser();
	$is_active = $this->request->getStringParam('is_active'); // Not needed when new is added
	$title = $this->request->getStringParam('title');
	$description = $this->request->getStringParam('description');
	$category = $this->request->getStringParam('category');


	$validation = $this->boardnotesModel->modelBoardnotesAddNote($project['id'], $user['id'], $is_active, $title, $description, $category);
    }

    public function BoardnotesAnalytic()
    {
        $project = $this->getProject();
        $user = $this->getUser();

	$analyticData = $this->boardnotesModel->modelBoardnotesAnalytics($project['id'], $user['id']);

	return $this->response->html($this->helper->layout->app('Boardnotes:boardnotes/analytics', array('title' => t('Analytics'),
                'project' => $project,
		'analyticData' => $analyticData
                )));
    }


     public function BoardnotesTask()
     {
	$rr = "lort";
	//$pp = array();
	//$pp = $this->taskMetadataModel->exists('25', 'key2');
	//return $this->taskMetadataModel->save($task_id, ['my_plugin_variable' => 'something']);
	$pp = $this->boardnotesModel->modelBoardnotes();
	$project = $this->getProject();
        //$task = $this->getTask();
	//$task_id = "25";
        //$metadata = $this->taskMetadataModel->getAll($task_id);

        $this->response->html($this->helper->layout->app('Boardnotes:project_overview/show', array('title' => t('Metadata'),
                'project' => $project,
		'rr' => $rr,
		'pp' => $pp)));

     }


    public function BoardnotesUpdatePosition()
    {
        $nrNotes = $this->request->getStringParam('nrNotes');
        $notePositions = $this->request->getStringParam('order');

        $validation = $this->boardnotesModel->modelBoardnotesUpdatePosition($notePositions, $nrNotes);

    }


}
