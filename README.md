Kanboard Plugin for notes
==========================

This plugin adds a GUI for notes.

Plugin for https://github.com/fguillot/kanboard

Author
------

- TTJ
- License MIT

Installation
------------

- Decompress the archive in the `plugins` folder

or

- Create a folder **plugins/Boardnotes**
- Copy all files under this directory

or

- Clone folder with git

Use
---

Take notes on the fly. Developed to easily take project specific notes. The purpose of the notes is not to be tasks, but for keeping information - a flexible alternative to metadata.
I'm using Kanboard as projectmanagement tool for managing construction projects, where I often need to take notes regarding specific installations, during site-visits or phonemeetings.

The notes is accessible from the project dropdown, where only the project specific notes will be shown. On the dashboard there's a link in the sidebar to view all notes, the notes will be separated in tabs.

Features
--------

- Take notes quickly. Write the note title and press ENTER to save.
- Press TAB in the new note title to show the detailed menu
- Add detailed description to new notes
- Add a category to notes. The category is the same as the projects categories. (Please see the section on bugs)
- Get pie analytic on open and done notes
- Delete all done notes
- One-click for editing notes status (open/done)
- Edit note title. Click on title, edit and press ENTER
- Press the show more button on a note to see the note details
- Edit an existing notes description. Click on the description, type, press TAB to save
- Change category on existing notes. If you want to remove the category, just choose option 2 (the blank)
- Free sorting. Move the notes around. The sorting is saved.

Todo
----

- Implement fault procedures (verify it is number, etc.)
- Adding possibility to attach image from mobile
- Finish exporting notes to task

Security issues and bugs
------------------------

- Focus on description textarea when pressing TAB on new notes title is not working
- Category is saved as text and does not have foreing key to the projects real category table
- Analytic chart on categories not developed
- Margin bottom not added
- The only folder in the `Template` folder is `boardnotes`, and not specified out on `dashboard` etc.
- There is no description of shortcuts (ENTER and TAB key)
- Delete directly on trash button on single note - to fast?
- Category not updating in title after manually changing the category
- Export note to real task in boardview permanent and delete note not developed
- If note has empty title, it's not possible to change it afterwards
- Analytic is breaking when viewing all projects (js not reloading correctly)

Tested on
---------

- Application version: 1.0.30
- PHP version: 5.5.9-1ubuntu4.17
- PHP SAPI: apache2handler
- OS version: Linux 3.13.0-74-generic
- Database driver: sqlite
- Database version: 3.8.2
- Browser: Chromium 51.0.2704.106 (64-bit)
