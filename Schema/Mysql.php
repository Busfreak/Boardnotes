<?php

namespace Kanboard\Plugin\Boardnotes\Schema;

use PDO;

const VERSION = 7;

function version_7(PDO $pdo)
{
    $pdo->exec("ALTER TABLE boardnotes ADD COLUMN category TEXT");
}


function version_6($pdo)
{
    $pdo->exec('CREATE TABLE IF NOT EXISTS boardnotes (
        `id` INT NOT NULL AUTO_INCREMENT,
        `project_id` INT NOT NULL,
        `user_id` INT NOT NULL,
        `position` INT,
        `is_active` INT,
        `title` TEXT,
        `description` TEXT,
        `date_created` INT,
        `date_modified` INT,
        FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
        PRIMARY KEY(id)
    ) ENGINE=InnoDB CHARSET=utf8');
}

