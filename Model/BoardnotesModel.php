<?php

namespace Kanboard\Plugin\Boardnotes\Model;

use PDO;
use Kanboard\Core\Base;
use Kanboard\Controller\BaseController;


class BoardnotesModel extends Base
{

	const TABLEnotes = 'boardnotes';
	const TABLEaccess = 'project_has_users';
	const TABLEcategories = 'project_has_categories';
	const TABLEprojects = 'projects';
	const TABLEtasks = 'tasks';

	public function modelBoardnotesShowNote($note_id) // Show single note
        {

        return $this->db
                ->table(self::TABLEnotes)
                ->eq('id', $note_id)
                ->findAll();

        }


	public function modelBoardnotesShowProject($project_id, $user_id) // Show all notes related to project
        {

        return $this->db
                ->table(self::TABLEnotes)
		->eq('user_id', $user_id)
		->eq('project_id', $project_id)
		->desc('is_active')
		->desc('position')
                ->findAll();

        }

	public function modelBoardnotesGetProjectid($user_id) // Get all project_id where user has access
        {

        return $this->db
                ->table(self::TABLEaccess)
		->columns(
			self::TABLEaccess.'.project_id',
			'tblPro.name AS project_name'
		)
		->eq('user_id', $user_id)
		->left(self::TABLEprojects, 'tblPro', 'id', self::TABLEaccess, 'project_id')
		->asc('project_id')
                ->findAll();
        }

        public function modelBoardnotesGetCategories($project_id) // Get all project_id where user has access
        {
/*
	foreach($projectAccess as $u) $uids[] = $u['project_id'];
                $projectAccess = implode(", ",$uids);
                substr_replace($projectAccess, "", -2);
                $projectAccess = explode(', ', $projectAccess);
*/
        return $this->db
                ->table(self::TABLEcategories)
                ->columns(
                        self::TABLEcategories.'.name',
                        self::TABLEcategories.'.project_id'
                )
 		//->in(self::TABLEcategories.'.project_id', $projectAccess)
		->eq('project_id', $project_id)
                ->asc('name')
                ->findAll();
        }


	public function modelBoardnotesShowAll($projectAccess, $user_id) // Show all notes
        {

	foreach($projectAccess as $u) $uids[] = $u['project_id'];
		$projectAccess = implode(", ",$uids);
		substr_replace($projectAccess, "", -2);
		$projectAccess = explode(', ', $projectAccess);

	return $this->db
                ->table(self::TABLEnotes)
		->eq('user_id', $user_id)
		->in(self::TABLEnotes.'.project_id', $projectAccess)
		->desc('project_id')
		->desc('is_active')
		->desc('position')
                ->findAll();
        }

	public function modelBoardnotesDeleteNote($note_id, $user_id) // Delete note
        {

	return $this->db
		->table(self::TABLEnotes)
		->eq('id', $note_id)
		->eq('user_id', $user_id)
		->remove();

        }


	public function modelBoardnotesDeleteAllDone($project_id, $user_id) // Delete note
        {

	return $this->db
		->table(self::TABLEnotes)
		->eq('project_id', $project_id)
		->eq('user_id', $user_id)
		->eq('is_active', "0")
		->remove();

        }

	public function modelBoardnotesUpdateNote($user_id, $note_id, $is_active, $title, $description, $category) // Update note
        {

	// Get current unixtime
        $t = time();

        $values = array(
                        'is_active' => $is_active,
                        'title' => $title,
                        'description' => $description,
			'category' => $category,
                        'date_modified' => $t,
        );

        return $this->db
                ->table(self::TABLEnotes)
                ->eq('id', $note_id)
		->eq('user_id', $user_id)
                ->update($values);

        }


	public function modelBoardnotesAddNote($project_id, $user_id, $is_active, $title, $description, $category) // Add note
        {

	// Get last position number
	$lastPosition = $this->db
		->table(self::TABLEnotes)
		->eq('project_id', $project_id)
		->desc('position')
		->findOneColumn('position');

	// Add 1 to position
	$lastPosition++;

	// Get current unixtime
	$t = time();

	// Define values
        $values = array(
                'project_id' => $project_id,
                'user_id' => $user_id,
                'position' => $lastPosition,
                'is_active' => $is_active,
                'title' => $title,
                'description' => $description,
		'date_created' => $t,
		'date_modified' => $t,
		'category' => $category,
        );

        return $this->db
                ->table(self::TABLEnotes)
                ->insert($values);

        }



	public function modelBoardnotesUpdatePosition($notePositions, $nrNotes) // Update note positions
        {

	unset($num);
	unset($note_id);

	// Ser $num to nr of notes to max
	$num = $nrNotes;

	//  Explode all positions
	$note_id = explode(',', $notePositions);

	// Loop through all positions
	foreach ($note_id as $row){

	        $values = array(
		        'position' => $num,
	        );

	        $this->db
	                ->table(self::TABLEnotes)
	                ->eq('id', $row)
	                ->update($values);

		$num--;
	}
        }


	public function modelBoardnotesAddNoteToTask($project_id, $user_id, $is_active, $title, $description, $category) // Add note to tasks table
        {
	// Get category id from name
	$category_id = $this->db
                ->table(self::TABLEcategories)
                ->eq('project_id', $project_id)
		->eq('name', $category)
                ->findOneColumn('column_id');

	// Get first column_id
	$firstColumnId = $this->db
                ->table(self::TABLEtasks)
                ->eq('project_id', $project_id)
		->asc('column_id')
                ->findOneColumn('column_id');

	// Get last position number
        $lastPosition = $this->db
                ->table(self::TABLEtasks)
                ->eq('project_id', $project_id)
		->eq('column_id', $firstColumnId)
                ->desc('position')
                ->findOneColumn('position');

        // Add 1 to position
        $lastPosition++;

        // Get current unixtime
        $t = time();

        // Define values
        $values = array(
		'title' => $title,
                'description' => $description,
                'date_creation' => $t,
                'color' => 'yellow',
                'project_id' => $project_id,
                'owner_id' => $user_id,
                'position' => $lastPosition,
                'is_active' => $is_active,
		'score' => '0',
		'date_due' => '0',
		'category' => $category_id,
		'creator_id' => $user_id,
		'date_modified' => $t,
		'date_started' => '0',
		'time_spent' => '0',
		'time_estimated' => '0',
		'swimlane_id' => '0',
		'date_moved' => $t,
		'recurrence_status' => '0',
		'recurrence_trigger' => '0',
		'recurrence_factor' => '0',
		'recurrence_timeframe' => '0',
		'recurrence_basedate' => '0',
		'priority' => '0'
        );

        return $this->db
                ->table(self::TABLEtasks)
                ->insert($values);
        }


	public function modelBoardnotesAnalytics($project_id, $user_id) // Delete note
        {

        return $this->db
                ->table(self::TABLEnotes)
                ->eq('project_id', $project_id)
                ->eq('user_id', $user_id)
                ->findAll();
        }


}

